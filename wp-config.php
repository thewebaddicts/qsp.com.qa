<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'qsp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#P){YY(R<]E`EkW]Y+COZrw`3:?I27*5zQSzoFjydwgf/43xEQpcr?2$yUvjd&Be' );
define( 'SECURE_AUTH_KEY',  'f O]Q3zzbNI rZkQvRisH Z6uaqdN/a>sizPZ!P/rjUe@wbGO8sl,J(CHKafz>#7' );
define( 'LOGGED_IN_KEY',    '&m_sD.cd<N|S=/W:DX3>z)X]qSrzLm&/2LY3K=n A]T+x-WW8aIX6.q/ r`989&3' );
define( 'NONCE_KEY',        '  W8aS+g[#24bZ(m+mfF=XAUwZxYr635ZVEgITj$Ez9[{=LB+//,Y87H4K7d<yk~' );
define( 'AUTH_SALT',        '>W:zM?E`{gD0f]yV ^Jz<ANazy,;~/Q{Hs81tEgzeat,GvH nmsnG^^t,0?/:IX>' );
define( 'SECURE_AUTH_SALT', 'E:IigV#j8E~AKwe6:in4<!391a?/5VDWzSq{`N8FD;@ZtSm)y8yQ,B/!Y]t<OUYj' );
define( 'LOGGED_IN_SALT',   '*w6wx;tNg+a?G?aq|iB5Ce24NuF$1,R`e|Y{:zLKA1~,8tLf 4pKS^#ZzQ%@sFx`' );
define( 'NONCE_SALT',       '#M~Yd,C5EK?pr(jw3-I2?4c]Dh=cl[j;0H!u!Emx,Fd)r6Lxo^E7w:wd343enk*C' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
