WebP Express 0.18.3. Conversion triggered using bulk conversion, 2020-11-07 12:38:22

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.3.11
- Server software: nginx/1.17.3

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: version: *1.0.3*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- /usr/local/bin/cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Darwin)... We do.
Found 1 binaries: 
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14
Detecting versions of the cwebp binaries found
- Executing: cwebp -version 2>&1. Result: version: *1.0.3*
- Executing: /usr/local/bin/cwebp -version 2>&1. Result: version: *1.0.3*
- Executing: [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14 -version 2>&1. Result: *Exec failed*. Permission denied (user: "assaadharb" does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 1.0.3)
- /usr/local/bin/cwebp: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice cwebp -metadata none -q 85 -alpha_q '85' -m 6 -low_memory '[doc-root]/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png' -o '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png.webp.lossy.webp' 2>&1 2>&1

*Output:* 
nice: cwebp: No such file or directory

Exec failed (return code: 127)
Creating command line options for version: 1.0.3
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice /usr/local/bin/cwebp -metadata none -q 85 -alpha_q '85' -m 6 -low_memory '[doc-root]/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png' -o '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png.webp.lossy.webp' 2>&1 2>&1

*Output:* 
Saving file '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png.webp.lossy.webp'
File:      [doc-root]/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png
Dimension: 1160 x 870
Output:    3058 bytes Y-U-V-All-PSNR 59.73 62.31 57.82   59.65 dB
           (0.02 bpp)
block count:  intra4:         22  (0.55%)
              intra16:      3993  (99.45%)
              skipped:      3748  (93.35%)
bytes used:  header:             26  (0.9%)
             mode-partition:   2467  (80.7%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |      52 |       0 |       6 |       0 |      58  (1.9%)
 intra16-coeffs:  |      21 |       1 |      20 |       4 |      46  (1.5%)
  chroma coeffs:  |     246 |      16 |     132 |      39 |     433  (14.2%)
    macroblocks:  |       7%|       0%|       7%|      86%|    4015
      quantizer:  |      20 |      20 |      14 |      13 |
   filter level:  |       7 |       4 |       2 |       2 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |     319 |      17 |     158 |      43 |     537  (17.6%)

Success
Reduction: -13% (went from 2708 bytes to 3058 bytes)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: version: *1.0.3*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- /usr/local/bin/cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Darwin)... We do.
Found 1 binaries: 
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14
Detecting versions of the cwebp binaries found
- Executing: cwebp -version 2>&1. Result: version: *1.0.3*
- Executing: /usr/local/bin/cwebp -version 2>&1. Result: version: *1.0.3*
- Executing: [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14 -version 2>&1. Result: *Exec failed*. Permission denied (user: "assaadharb" does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 1.0.3)
- /usr/local/bin/cwebp: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Trying to convert by executing the following command:
nice cwebp -metadata none -q 85 -alpha_q '85' -near_lossless 60 -m 6 -low_memory '[doc-root]/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png' -o '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png.webp.lossless.webp' 2>&1 2>&1

*Output:* 
nice: cwebp: No such file or directory

Exec failed (return code: 127)
Creating command line options for version: 1.0.3
Trying to convert by executing the following command:
nice /usr/local/bin/cwebp -metadata none -q 85 -alpha_q '85' -near_lossless 60 -m 6 -low_memory '[doc-root]/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png' -o '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png.webp.lossless.webp' 2>&1 2>&1

*Output:* 
Saving file '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png.webp.lossless.webp'
File:      [doc-root]/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png
Dimension: 1160 x 870
Output:    798 bytes (0.01 bpp)
Lossless-ARGB compressed size: 798 bytes
  * Header size: 132 bytes, image data size: 640
  * Lossless features used: PALETTE
  * Precision Bits: histogram=5 transform=4 cache=0
  * Palette size:   83

Success
Reduction: 71% (went from 2708 bytes to 798 bytes)

Picking lossless
cwebp succeeded :)

Converted image in 495 ms, reducing file size with 71% (went from 2708 bytes to 798 bytes)
