WebP Express 0.18.3. Conversion triggered using bulk conversion, 2020-11-07 12:38:17

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.3.11
- Server software: nginx/1.17.3

Stack converter ignited
Destination folder does not exist. Creating folder: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: version: *1.0.3*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- /usr/local/bin/cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Darwin)... We do.
Found 1 binaries: 
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14
Detecting versions of the cwebp binaries found
- Executing: cwebp -version 2>&1. Result: version: *1.0.3*
- Executing: /usr/local/bin/cwebp -version 2>&1. Result: version: *1.0.3*
- Executing: [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14 -version 2>&1. Result: *Exec failed*. Permission denied (user: "assaadharb" does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 1.0.3)
- /usr/local/bin/cwebp: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice cwebp -metadata none -q 85 -alpha_q '85' -m 6 -low_memory '[doc-root]/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png' -o '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png.webp.lossy.webp' 2>&1 2>&1

*Output:* 
nice: cwebp: No such file or directory

Exec failed (return code: 127)
Creating command line options for version: 1.0.3
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice /usr/local/bin/cwebp -metadata none -q 85 -alpha_q '85' -m 6 -low_memory '[doc-root]/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png' -o '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png.webp.lossy.webp' 2>&1 2>&1

*Output:* 
Saving file '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png.webp.lossy.webp'
File:      [doc-root]/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png
Dimension: 2880 x 1400
Output:    8636 bytes Y-U-V-All-PSNR 64.03 62.58 61.76   63.31 dB
           (0.02 bpp)
block count:  intra4:         11  (0.07%)
              intra16:     15829  (99.93%)
              skipped:     15491  (97.80%)
bytes used:  header:             36  (0.4%)
             mode-partition:   7952  (92.1%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |      46 |       0 |       0 |       0 |      46  (0.5%)
 intra16-coeffs:  |      25 |       0 |       0 |       1 |      26  (0.3%)
  chroma coeffs:  |     411 |       0 |       0 |     134 |     545  (6.3%)
    macroblocks:  |       5%|       0%|       0%|      95%|   15840
      quantizer:  |      20 |      20 |      18 |      13 |
   filter level:  |       7 |       5 |       3 |       2 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |     482 |       0 |       0 |     135 |     617  (7.1%)

Success
Reduction: -38% (went from 6248 bytes to 8636 bytes)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: version: *1.0.3*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- /usr/local/bin/cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Darwin)... We do.
Found 1 binaries: 
- [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14
Detecting versions of the cwebp binaries found
- Executing: cwebp -version 2>&1. Result: version: *1.0.3*
- Executing: /usr/local/bin/cwebp -version 2>&1. Result: version: *1.0.3*
- Executing: [doc-root]/wp-content/plugins/webp-express/vendor/rosell-dk/webp-convert/src/Convert/Converters/Binaries/cwebp-103-mac-10_14 -version 2>&1. Result: *Exec failed*. Permission denied (user: "assaadharb" does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 1.0.3)
- /usr/local/bin/cwebp: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Trying to convert by executing the following command:
nice cwebp -metadata none -q 85 -alpha_q '85' -near_lossless 60 -m 6 -low_memory '[doc-root]/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png' -o '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png.webp.lossless.webp' 2>&1 2>&1

*Output:* 
nice: cwebp: No such file or directory

Exec failed (return code: 127)
Creating command line options for version: 1.0.3
Trying to convert by executing the following command:
nice /usr/local/bin/cwebp -metadata none -q 85 -alpha_q '85' -near_lossless 60 -m 6 -low_memory '[doc-root]/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png' -o '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png.webp.lossless.webp' 2>&1 2>&1

*Output:* 
Saving file '[doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png.webp.lossless.webp'
File:      [doc-root]/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png
Dimension: 2880 x 1400
Output:    1648 bytes (0.00 bpp)
Lossless-ARGB compressed size: 1648 bytes
  * Header size: 176 bytes, image data size: 1446
  * Lossless features used: PALETTE
  * Precision Bits: histogram=6 transform=4 cache=0
  * Palette size:   55

Success
Reduction: 74% (went from 6248 bytes to 1648 bytes)

Picking lossless
cwebp succeeded :)

Converted image in 1312 ms, reducing file size with 74% (went from 6248 bytes to 1648 bytes)
