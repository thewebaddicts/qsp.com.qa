<?php /* Template Name: Template | Search Page */ ?>
<?php get_header(); ?>


<?php //$Banner = get_fields('24'); ?>
<?php //$youtubeID = extractYoutubeID($Banner['video_url']); ?>


<?php


global $query_string;

wp_parse_str( $query_string, $search_query );
if(is_array($search_query) && array_key_exists("s",$search_query)){
    $search = $search_query['s'];
}else{
    $search = "";
}
?>


<div class="menu-spacer"></div>
<div class="content-small">
    <div class="form-search">
        <form role="search" method="get" id="searchform" class="searchform">
                <input type="text" value="<?php echo $search; ?>" name="s" id="s" placeholder="search QSP ..." class="browser-default">
            <button type="submit" id="searchsubmit"><span class="material-icons">search</span></button>
        </form>
    </div>

    <?php if($search!=''){
        $search1 = new WP_Query( array(
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => 'content.php',
            's' => $search
        ) );

        $search2 = new WP_Query( array(
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => array('templates/MediaAndParagraph.php','templates/Grid.php'),
            's' => $search,
        ) );

        $search3 = new WP_Query( array(
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => array('templates/MediaAndParagraph.php','templates/Grid.php'),
            'meta_query' => array(
                array(
                    'value' => $search
                )
            )
        ) );

        $result = new WP_Query();
        $result->posts = array_unique( array_merge( $search2->posts, $search3->posts ), SORT_REGULAR );
        $result->post_count = count( $result->posts );



            ?>
            <div class="search-results">

                <h4 data-aos="fade-up"><?php echo count($result->posts) + count($search1->posts); ?> results found</h4>


                <?php $i=0; foreach ($search1->posts AS $post){ $i++;   ?>
                    <a href="<?php echo $post->guid; ?>" data-aos="fade-up" data-aos-delay="<?php echo $i*100; ?>">
                        <span><?php echo get_the_title( $post->post_parent ); ?></span>
                        <h5><?php echo $post->post_title; ?></h5>
                    </a>
                <?php } ?>



                <?php foreach ($result->posts AS $post){ ?>
                    <a href="<?php echo get_permalink( $post->post_parent );  ?>">
                        <span><?php echo get_the_title( $post->post_parent ); ?></span>
                        <h5><?php echo $post->post_title; ?></h5>
                        <span><?php echo $post->post_content; ?></span>
                    </a>
                <?php } ?>

            </div>
            <?php
    } ?>


</div>

<?php get_footer(); ?>


