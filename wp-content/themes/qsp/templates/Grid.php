<?php /* Template Name: Template | Grid Display */ ?>
<?php $meta = get_post_meta(get_the_ID()); ?>

<?php
switch ($meta['box_type'][0]){
    case "Big Icon Box":
    {
        $BoxClass = "icon-big";
        break;
    }
    case "Small Icon Box":
    {
        $BoxClass = "icon-small";
        break;
    }
    case "Image Box":
    {
        $BoxClass = "image";
        break;
    }
    default:
    {
        $BoxClass = "no-icon no-picture";
    }
}

switch ($meta['background_color'][0]){
    case "Light Blue":
        $ClassBG = "LightBlue";
        break;
    default:
        $ClassBG="VeryLightBlue";
}


?>
<?php if($meta['has_background'][0]==1){ ?>
    <div class="content_grid parallax-window" data-parallax="scroll" data-image-src="<?php echo wp_get_attachment_image_src($meta['background_image'][0])[0]; ?>" data-aos="fade-in">
<?php }else{ ?>
    <div class="content_grid <?php echo $ClassBG; ?>" data-aos="fade-in">
<?php } ?>

    <div class="row content-small text-center header">
        <div class="col s12 text" data-aos="fade-up">
            <h1 data-aos="fade-up" data-aos-delay="100"><?php the_title(); ?></h1>
            <div class="text" data-aos="fade-up" data-aos-delay="200"><?php the_content(); ?></div>
        </div>
    </div>


    <div class="content row left-align">
        <?php for($i=0;$i<$meta['content'][0];$i++){ ?>
            <div class="box <?php echo $BoxClass; ?> col <?php echo str_replace(['1/4','1/3','1/2','1/1'],['m3 s6','m4 s12','m6 s12','m12 s12'],$meta['content_'.$i.'_size'][0]); ?>" data-aos="fade-up" data-aos-delay="<?php echo $i*100+300; ?>">
                <div class="inner">
                    <?php if($BoxClass == "image"){ ?>
                        <div class="the_image" style="background-image: url('<?php echo wp_get_attachment_image_src($meta['content_'.$i.'_image'][0])[0]; ?>')"></div>
<!--                        <div class="the_arrow"><ispan class="material-icons arrow">arrow_forward</ispan></div>-->
                    <?php } ?>
                    <div class="the_content">
                        <?php if(array_key_exists('content_'.$i.'_icon',$meta)){ ?><div class="icon"><?php echo $meta['content_'.$i.'_icon'][0]; ?></div><?php } ?>
                        <h3><?php echo $meta['content_'.$i.'_title'][0]; ?></h3>
                        <?php if($meta['content_'.$i.'_text'][0]!=''){  ?>
                            <div class="text"><?php echo $meta['content_'.$i.'_text'][0]; ?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<!--<pre>-->
<?php //print_r($meta);  ?>
<!--</pre>-->
