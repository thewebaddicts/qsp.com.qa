<?php /* Template Name: Template | Media & Paragraph */ ?>
<?php $meta = get_post_meta(get_the_ID()); ?>


<?php
$class="";
switch ($meta['display_method'][0]){
    case "Media on Top, Text Under the Media":
        {
        $ClassImg = "s12 text-center";
        $ClassText = "s12";
        $ClassContainer = "content-small text-center";
        break;
    }
    case "Text on Top, Media under the Text":
    {
        $ClassImg = "s12 text-center";
        $ClassText = "s12";
        $ClassContainer = "content-small text-center";
        break;
    }
    case "Media on the Right, Paragraph to the Left":
    {
        $ClassImg = "s12 m6 right";
        $ClassText = "s12 m6 left";
        $ClassContainer = "content";
        break;
    }
    case "Media on the Left, Paragraph to the Right":
    {
        $ClassImg = "s12 m6 left";
        $ClassText = "s12 m6 right";
        $ClassContainer = "content";
        break;
    }
    default:
    {
        $ClassImg = "s12 text-center";
        $ClassText = "s12 text-center";
        $ClassContainer = "content-small";
    }
}

if(array_key_exists('background_color',$meta)){
    switch ($meta['background_color'][0]){
        case "Light Blue":
            $ClassBG = "LightBlue";
            break;
        default:
            $ClassBG="VeryLightBlue";
    }
}else{
    $ClassBG="VeryLightBlue";
}




if(array_key_exists("image",$meta) && isset($meta['image'][0]) && $meta['image'][0]!='' && $meta['image'][0] != NULL){
    $media = '<img src="'.wp_get_attachment_image_src($meta['image'][0])[0].'">';
}else{
    $media = "<div class='video-container''>".wp_oembed_get($meta['video'][0], [ "width"=> "580"])."</div>";
}
?>
<?php if($meta['has_background'][0]==1){ ?>
    <div class="media_and_paragraph <?php echo $class; ?> parallax-window" data-parallax="scroll" data-image-src="<?php echo wp_get_attachment_image_src($meta['background_image'][0])[0]; ?>" data-aos="fade-in">
<?php }else{ ?>
    <div class="media_and_paragraph <?php echo $class; ?> <?php echo $ClassBG; ?>" data-aos="fade-in">
<?php } ?>


    <?php if(($meta['display_method'][0] == "Media on Top, Text Under the Media" || $meta['display_method'][0] == "Text on Top, Media under the Text") && $meta['media_type_is'][0]!='no-media'){ ?>
        <?php if($meta['display_method'][0] == "Media on Top, Text Under the Media"){ ?>
            <div class="row content" data-aos="fade-up" data-aos-delay="100">
                <div class="col <?php echo $ClassImg; ?> media "><?php echo $media; ?></div>
                <sep></sep><sep></sep>
            </div>
        <?php } ?>
        <div class="row <?php echo $ClassContainer; ?>">
            <div class="col <?php echo $ClassText; ?> text">
                <h1  data-aos="fade-up" data-aos-delay="100"><?php the_title(); ?></h1>
                <div class="text-gray"  data-aos="fade-up" data-aos-delay="200"><?php the_content(); ?></div>
            </div>
        </div>
        <?php if($meta['display_method'][0] != "Media on Top, Text Under the Media"){ ?>
            <div class="row content"  data-aos="fade-up" data-aos-delay="300">
                <div class="col <?php echo $ClassImg; ?> media video-container"><?php echo $media; ?></div>
            </div>
        <?php } ?>

<?php }else{ ?>
        <div class="row <?php echo $ClassContainer; ?>">
            <div class="col <?php echo $ClassText; ?> text">
                <h1  data-aos="fade-up" data-aos-delay="200"><?php the_title(); ?></h1>
                <div class="text-gray"  data-aos="fade-up" data-aos-delay="300"><?php the_content(); ?></div>
            </div>
            <?php if($meta['media_type_is'][0]!='no-media'){ ?>
            <div class="col <?php echo $ClassImg; ?> media"><?php echo $media; ?></div>
            <?php } ?>
        </div>
<?php } ?>



</div>
