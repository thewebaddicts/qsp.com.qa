<?php /* Template Name: Template | Contact Us */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>


<div class="menu-spacer"></div>
<div class="content-banner parallax-window <?php if($meta['page_large_header'][0]!=1){ echo "small"; } ?> <?php if($meta['page_is_centered'][0]==1){ echo "text-center"; } ?>" data-parallax="scroll" data-image-src="<?php echo wp_get_attachment_image_src($meta['page_header_image'][0])[0]; ?>" data-aos="fade-in">
    <div class="content">
        <div class="banner-content">
            <h1 data-aos="fade-right" data-aos-delay="100"><?php echo $meta['page_header_title'][0]; ?></h1>
            <div class="subtitle" data-aos="fade-right" data-aos-delay="200"><?php echo $meta['page_header_text'][0]; ?></div>
        </div>
    </div>
</div>

<div class="contact-info">
    <div class="content">
        <div class="row">
            <div class="s12 m12 col info" data-aos="fade-up" data-aos-delay="100"><?php the_content(); ?></div>
        </div>
        <div class="row">
            <?php for($i=0;$i<$meta['small_paragraphs'][0];$i++){ ?>
            <div class="col s12 m6 l4 contact-box" data-aos="fade-up" data-aos-delay="<?php echo 300+$i*100; ?>">
                <h3><?php echo $meta['small_paragraphs_'.$i.'_title'][0]; ?></h3>
                <div class="text-gray"><?php echo $meta['small_paragraphs_'.$i.'_text'][0]; ?></div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<div id="map" style="min-height:500px;" data-aos="fade-in"></div>

<div class="contact-subscribe">
<div class="content">
    <div class="row">
        <div class="col s12 m6 left"><h2>Subscribe to our newsletter and stay updated with our latest news</h2></div>
        <div class="col s12 m6 right">
            <form method="post" class="footer-newsletter form-white">
                <input type="email" class="browser-default" name="email" placeholder="Email Address"  required>
                <button type="submit" class="btn-flat"><span class="material-icons arrow">arrow_forward</span></button>
            </form>
        </div>
    </div>
</div>
</div>




<!--<pre>--><?php //print_r($meta); ?><!--</pre>-->

<script language="javascript">
    // eslint-disable no-undef
let map;

// Initialize and add the map
function initMap() {
    const myLatLng = { lat: <?php echo $meta['latitude'][0]; ?>, lng: <?php echo $meta['longitude'][0]; ?> };
    map = new google.maps.Map(document.getElementById("map"), {
    center: myLatLng,
    zoom: 17,
    styles: [
        {
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#f5f5f5"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#bdbdbd"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e5e5e5"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#dadada"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e5e5e5"
                }
            ]
        },
        {
            "featureType": "transit.station",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#c9c9c9"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        }
    ]
    });



    new google.maps.Marker({
        position: myLatLng,
        map,
        title: "Hello World!",
        icon: "<?php echo get_template_directory_uri(); ?>/assets/images/pin.png"
    });


}
</script>
<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAov9Oth8kTG8Jt2yFqhhVlXadrY89bXnU&callback=initMap&libraries=&v=beta&map_ids=3a3b33f0edd6ed2a"
        defer
></script>
<?php get_footer(); ?>


