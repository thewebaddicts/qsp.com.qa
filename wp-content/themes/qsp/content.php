<?php /* Template Name: | Main Template for Solutions & Services */ ?>
<?php get_header(); ?>
<?php //$Banner = get_fields('24'); ?>
<?php //$youtubeID = extractYoutubeID($Banner['video_url']); ?>







<?php $meta = get_post_meta(get_the_ID()); ?>

<div class="menu-spacer"></div>
<div class="content-banner parallax-window <?php if($meta['page_large_header'][0]!=1){ echo "small"; } ?> <?php if($meta['page_is_centered'][0]==1){ echo "text-center"; } ?>" data-parallax="scroll" data-image-src="<?php echo wp_get_attachment_image_src($meta['page_header_image'][0])[0]; ?>" data-aos="fade-in">
    <div class="content">
        <div class="banner-content">
            <h1 data-aos="fade-right" data-aos-delay="100"><?php echo $meta['page_header_title'][0]; ?></h1>
            <div class="subtitle" data-aos="fade-right" data-aos-delay="200"><?php echo $meta['page_header_text'][0]; ?></div>
        </div>
    </div>
</div>

<?php  $pages = get_pages([ "child_of" => get_the_ID(), "parent" => get_the_ID(), "sort_column" => "menu_order" ]); ?>

<?php
foreach ($pages AS $page){
    $html = file_get_contents($page->guid); echo $html;
} ?>


<?php /*

<?php if(have_posts()) : ?>
    <?php while(have_posts())  : the_post(); ?>
        <h2><?php the_title(); ?></h2>
        <?php the_content(); ?>
        <?php comments_template( '', true ); ?>
    <?php endwhile; ?>
<?php else : ?>
    <h3><?php _e('404 Error&#58; Not Found'); ?></h3>
<?php endif; ?>


*/ ?>

<?php get_footer(); ?>

