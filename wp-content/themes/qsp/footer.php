<?php if(get_the_ID()!=5){?>
<div class="footer-contact">
    <div class="content content-small">
        <div class="title-group text-center content-smaller">
            <label data-aos="fade-up" data-aos-delay="0">Contact Us</label>
            <h2 class="" data-aos="fade-up" data-aos-delay="100">Stay in touch</h2>
        </div>
        <div class="text-center">
        <?php $content = apply_filters('the_content', get_post_field('post_content', 24)); print_r($content); ?></div>
    </div>
</div
<?php } ?>


<div class="footer-menu">
    <div class="content row blocks">

        <div class="col s3 hide-on-med-and-down"><h3>Site Map</h3><?php wp_nav_menu([
            "menu" => "footer",
            "theme_location" => "primary",
            "container" => "",
            "items_wrao" => "<ul></ul>",
            "depth" => 1,
        ]); ?></div>


        <div class="col s6 hide-on-med-and-down">
            <h3>Contact Info</h3>
            <?php $TopInfo = get_fields('24'); ?>
            <div class="footer-info">
                <div><b>A</b><?php echo $TopInfo['address'] ?></div>
                <a href="tel:<?php echo $TopInfo['telephone_number'] ?>" target="_blank"><b>T</b><?php echo $TopInfo['telephone_number'] ?></a>
                <a href="mailto:<?php echo $TopInfo['email'] ?>" target="_blank"><b>E</b><?php echo $TopInfo['email'] ?></a>
            </div>
        </div>

        <div class="col s3 right hide-on-med-and-down">
            <?php /* ?>
            <h3>Newsletter</h3>
            <span class="text-gray">Stay updated by subscribing to our newsletter</span>
            <form method="post" class="footer-newsletter form-white">
                <input type="email" class="browser-default" name="email" placeholder="Email Address"  required>
                <button type="submit" class="btn-flat"><span class="material-icons arrow">arrow_forward</span></button>
            </form>
            <?php */ ?>
            <h3>Follow Us</h3>
            <?php if(array_key_exists("facebook",$TopInfo) && array_key_exists("url",$TopInfo['facebook']) && $TopInfo['facebook']['url']!=''){ ?><a href="<?php echo $TopInfo['facebook']['url'] ?>" target="_blank" class="social" style="margin-left:-10px;"><?php echo htmlspecialchars_decode($TopInfo['facebook']['title']); ?></a><?php } ?>
            <?php if(array_key_exists("twitter",$TopInfo) && array_key_exists("url",$TopInfo['twitter']) && $TopInfo['twitter']!=''){ ?><a href="<?php echo $TopInfo['twitter']['url'] ?>" target="_blank" class="social"><?php echo htmlspecialchars_decode($TopInfo['twitter']['title']); ?></a><?php } ?>
            <?php if(array_key_exists("youtube",$TopInfo) && array_key_exists("url",$TopInfo['youtube']) && $TopInfo['youtube']!=''){ ?><a href="<?php echo $TopInfo['youtube']['url'] ?>" target="_blank" class="social"><?php echo htmlspecialchars_decode($TopInfo['youtube']['title']); ?></a><?php } ?>
            <?php if(array_key_exists("linkedin",$TopInfo) && array_key_exists("url",$TopInfo['linkedin']) && $TopInfo['linkedin']!=''){ ?><a href="<?php echo $TopInfo['linkedin']['url'] ?>" target="_blank" class="social"><?php echo htmlspecialchars_decode($TopInfo['linkedin']['title']); ?></a><?php } ?>
        </div>
    </div>
    <div class="content row"><div class="col s12 text-gray">Designed & Developed by The Web Addicts</div></div>
</div>


<!--<script language="javascript" src="--><?php //echo get_template_directory_uri().'/assets/js/youtube-bg/jquery.youtube-background.js'; ?><!--"></script>-->
<!--<script language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.js"></script>-->
<?php
wp_footer();
?>
<!--<script language="javascript" src="--><?php //echo get_template_directory_uri().'/assets/js/aos/aos.js'; ?><!--"></script>-->
<!--<script language="javascript" src="--><?php //echo get_template_directory_uri().'/assets/js/parallax-js/parallax.min.js'; ?><!--"></script>-->
<!--<script language="javascript" src="--><?php //echo get_template_directory_uri().'/assets/js/init.js'; ?><!--"></script>-->
</body>
</html>
