<?php /* Template Name: Template | Our Partners */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>


<div class="menu-spacer"></div>
<div class="content-banner parallax-window <?php if($meta['page_large_header'][0]!=1){ echo "small"; } ?> <?php if($meta['page_is_centered'][0]==1){ echo "text-center"; } ?>" data-parallax="scroll" data-image-src="<?php echo wp_get_attachment_image_src($meta['page_header_image'][0])[0]; ?>" data-aos="fade-in">
    <div class="content">
        <div class="banner-content">
            <h1 data-aos="fade-right" data-aos-delay="100"><?php echo $meta['page_header_title'][0]; ?></h1>
            <div class="subtitle" data-aos="fade-right" data-aos-delay="200"><?php echo $meta['page_header_text'][0]; ?></div>
        </div>
    </div>
</div>



<div class="partners-content">
    <div class="title-group text-center content-small text-white" data-aos="fade-up" data-aos-delay="100">
        <label data-aos="fade-up" data-aos-delay="200"><?php echo $meta['strategic_partners_label'][0]; ?></label>
        <h2  data-aos="fade-up" data-aos-delay="300"><?php echo $meta['strategic_partners_title'][0]; ?></h2>
    </div>


    <div class="carousel-container text-white">
        <div class="arrow-left" onclick="carouselPrev(this)"><i class="fas fa-chevron-left"></i></div>
        <div class="arrow-right" onclick="carouselNext(this)"><i class="fas fa-chevron-right"></i></div>

        <div class="carousel cards-carousel-blue" style="min-height: 600px;" data-aos="fade-up">
        <?php for($i=0;$i<$meta['strategic_partners_partners'][0];$i++){ ?>
        <section class="carousel-item">
            <div class="image" style="background-image: url('<?php echo wp_get_attachment_image_src($meta['strategic_partners_partners_'.$i.'_logo_gray'][0])[0]; ?>') "></div>
            <div class="line"></div>
            <h3><?php echo $meta['strategic_partners_partners_'.$i.'_partner_name'][0]; ?></h3>
            <div class="text"><?php echo $meta['strategic_partners_partners_'.$i.'_partner_description'][0]; ?></div>
            <a class="link" href="<?php echo $meta['strategic_partners_partners_'.$i.'_website'][0]; ?>"><?php echo $meta['strategic_partners_partners_'.$i.'_website'][0]; ?></a>
        </section>
        <?php } ?>
            <?php for($i=0;$i<$meta['long_term_partners_partners'][0];$i++){ ?>
                <section class="carousel-item">
                    <div class="image" style="background-image: url('<?php echo wp_get_attachment_image_src($meta['long_term_partners_partners_'.$i.'_logo_gray'][0])[0]; ?>') "></div>
                    <div class="line"></div>
                    <h3><?php echo $meta['long_term_partners_partners_'.$i.'_partner_name'][0]; ?></h3>
                    <div class="text"><?php echo $meta['long_term_partners_partners_'.$i.'_partner_description'][0]; ?></div>
                    <a class="link" href="<?php echo $meta['long_term_partners_partners_'.$i.'_website'][0]; ?>"><?php echo $meta['long_term_partners_partners_'.$i.'_website'][0]; ?></a>
                </section>
            <?php } ?>
        </div>
    </div>


</div>




<?php /*
<div class="partners-content">
    <div class="title-group text-center content-small text-white" data-aos="fade-up" data-aos-delay="100">
        <label data-aos="fade-up" data-aos-delay="200"><?php echo $meta['long_term_partners_label'][0]; ?></label>
        <h2  data-aos="fade-up" data-aos-delay="300"><?php echo $meta['long_term_partners_title'][0]; ?></h2>
    </div>


    <div class="carousel-container text-white">
        <div class="arrow-left" onclick="carouselPrev(this)"><i class="fas fa-chevron-left"></i></div>
        <div class="arrow-right" onclick="carouselNext(this)"><i class="fas fa-chevron-right"></i></div>

        <div class="carousel cards-carousel-blue">
            <?php for($i=0;$i<$meta['long_term_partners_partners'][0];$i++){ ?>
                <section class="carousel-item">
                    <div class="image" style="background-image: url('<?php echo wp_get_attachment_image_src($meta['long_term_partners_partners_'.$i.'_logo_gray'][0])[0]; ?>') "></div>
                    <div class="line"></div>
                    <h3><?php echo $meta['long_term_partners_partners_'.$i.'_partner_name'][0]; ?></h3>
                    <div class="text"><?php echo $meta['long_term_partners_partners_'.$i.'_partner_description'][0]; ?></div>
                    <a class="link" href="<?php echo $meta['long_term_partners_partners_'.$i.'_website'][0]; ?>"><?php echo $meta['long_term_partners_partners_'.$i.'_website'][0]; ?></a>
                </section>
            <?php } ?>
        </div>
    </div>

</div>
<!--<pre>--><?php //print_r($meta); ?><!--</pre>-->
<?php */ ?>

<?php get_footer(); ?>


