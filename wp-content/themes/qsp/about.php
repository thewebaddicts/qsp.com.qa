<?php /* Template Name: Template | About Page */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>



<?php //$Banner = get_fields('24'); ?>
<?php //$youtubeID = extractYoutubeID($Banner['video_url']); ?>

<div class="menu-spacer"></div>
<div class="content-banner parallax-window <?php if($meta['page_large_header'][0]!=1){ echo "small"; } ?> <?php if($meta['page_is_centered'][0]==1){ echo "text-center"; } ?>" data-parallax="scroll" data-image-src="<?php echo wp_get_attachment_image_src($meta['page_header_image'][0])[0]; ?>" data-aos="fade-in">
    <div class="content">
        <div class="banner-content">
            <h1  data-aos="fade-right" data-aos-delay="100"><?php echo $meta['page_header_title'][0]; ?></h1>
            <div class="subtitle" data-aos="fade-right" data-aos-delay="200"><?php echo $meta['page_header_text'][0]; ?></div>
        </div>
    </div>
</div>

<div class="about-profile" data-aos="fade-up" data-aos-delay="0">
    <div class="content">
        <div class="title-group text-center content-small">
            <label  data-aos="fade-up" data-aos-delay="100"><?php echo $meta['label'][0]; ?></label>
            <h2 class=""  data-aos="fade-up" data-aos-delay="200"><?php echo $meta['title'][0]; ?></h2>
        </div>
        <div class="row" style="margin-top:100px;">
            <div class="col s6 big_number_section"  data-aos="fade-in" data-aos-delay="200">
                <div class="number"  data-aos="fade-right" data-aos-delay="300"><?php echo $meta['number_big_number'][0]; ?></div>
                <div class="unit"  data-aos="fade-right" data-aos-delay="400"><?php echo $meta['number_big_unit'][0]; ?></div>
                <sep></sep><sep></sep>
                <a href="<?php echo unserialize($meta['number_big_button_link'][0])['url'] ?>" target="<?php echo unserialize($meta['number_big_button_link'][0])['targe'] ?>" class="learn_more waves-effect left"  data-aos="fade-right" data-aos-delay="500"><span><?php echo unserialize($meta['number_big_button_link'][0])['title']; ?></span> <span class="material-icons arrow">arrow_forward</span></a>
            </div>
            <div class="col s6 text-gray" data-aos="fade-up" data-aos-delay="500"><?php the_content(); ?></div>
        </div>





        <?php $Banner = get_fields('24'); ?>
        <div class="row">
            <div class="col s12 about-video zoom">
                <?php /*
                <a class="video" href="<?php echo $Banner['about_video_url']['url']; ?>" style="background-image: url('https://img.youtube.com/vi/<?php echo extractYoutubeID($Banner['about_video_url']['url']); ?>hqdefault.jpg')" data-aos="fade-up" data-aos-delay=""><i class="fas fa-play"></i></a>
                <?php */ ?>
                <a class="video" href="javascript:;" style="background-image: url('<?php echo get_template_directory_uri().'/assets/images/qsp.jpg'; ?>')" data-aos="fade-up" data-aos-delay=""></a>
                <?php /*
                <?php for($i=1;$i<=4;$i++){ ?>
                    <div class="image_<?php echo $i; ?>">
                        <div class="rellax" data-rellax-speed="<?php echo $i/2; ?>" style="background-image: url('<?php echo  $Banner['about_images_'.$i]['url']; ?>')"></div>
                    </div>
                <?php } ?>
 */ ?>
            </div>
        </div>


        <div class="row">
            <?php for($i=0;$i<$meta['accordion'][0];$i++){ ?>
            <div class="col s12 m6 l4" style="position:relative;">
                <div class="accordion" data-aos="fade-up" data-aos-delay="<?php echo $i*100; ?>">
                    <a href="javascript:;"><?php echo $meta['accordion_'.$i.'_title'][0]; ?></a>
                    <div class="text text-gray"><?php echo nl2br($meta['accordion_'.$i.'_text'][0]); ?></div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>


</div>






<?php  $pages = get_pages([ "child_of" => get_the_ID(), "parent" => get_the_ID(), "sort_column" => "menu_order" ]); ?>

<?php
foreach ($pages AS $page){
    $html = file_get_contents($page->guid); echo $html;
} ?>



<?php
$meta_clients = get_post_meta(249);
if($meta_clients['customers_list'][0]>6){
    $meta_clients['customers_list'][0] = 6;
}

?>
<div class="about-customers" data-aos="fade-up">
    <div class="row content">
        <?php for($i=0;$i<$meta_clients['customers_list'][0];$i++){ ?>
            <div class="col s4 m3 l2">
                <a class="logo" href="<?php echo $meta_clients['customers_list_'.$i.'_website'][0]; ?>" target="_blank" title="<?php echo $meta_clients['customers_list_'.$i.'_title'][0]; ?>" style="background-image: url('<?php echo wp_get_attachment_image_src($meta_clients['customers_list_'.$i.'_logo'][0])[0]; ?>')"></a>
            </div>
        <?php } ?>
    </div>
</div>


<?php /* ?>
<div class="homepage-testimonials parallax-window" data-parallax="scroll" data-image-src="<?php echo $Banner['testimonials_background_image']; ?>" data-aos="fade-in">
    <div class="content">
        <div class="title-group text-center content-smaller" data-aos="fade-up">
            <label data-aos="fade-up" data-aos-delay="100"><?php echo $Banner['testimonials_label']; ?></label>
            <h2 class="text-white" data-aos="fade-up" data-aos-delay="200"><?php echo $Banner['testimonials_tittle']; ?></h2>
        </div>
        <div class="subtitle text-white"  data-aos="fade-up" data-aos-delay="300"><?php echo $Banner['testimonials_description']; ?></div>
        <sep></sep><sep></sep><sep></sep><sep></sep>
        <div class="carousel cards-carousel-full dark" style="min-height: 300px;"  data-aos="fade-up" data-aos-delay="400">
            <?php foreach (get_posts([ "category_name" => "Testimonial"]) AS $testimonial){ $PostMeta = get_post_meta($testimonial->ID);  ?>
                <section class="carousel-item" href="javascript:;">
                    <div class="image" style="background-image: url('<?php echo wp_get_attachment_image_src($PostMeta['image'][0])[0]; ?>')"></div>
                    <div class="right">
                        <h3><?php echo $PostMeta['full_name'][0]; ?></h3>
                        <div class=""><?php echo $PostMeta['position'][0]; ?></div>
                    </div>
                    <div class="text"><?php echo $PostMeta['testimonial'][0]; ?></div>
                </section>
            <?php } ?>
        </div>
    </div>
</div>
<?php */ ?>





<?php get_footer(); ?>


