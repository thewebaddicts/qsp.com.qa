<?php get_header(); ?>
<?php $Banner = get_fields('24'); ?>
<?php $youtubeID = extractYoutubeID($Banner['video_url']); ?>

<?php $rand=rand(0,count($Banner['slide_info'])-1); ?>
<div class="menu-spacer"></div>
<div class="homepage-banner" data-aos="fade-in">
    <div id="ytplayer" data-youtube="<?php echo $Banner['slide_info'][$rand]['video_url']; ?>"></div>
    <div class="content">
        <h1 data-aos="fade-right" data-aos-delay="100"><?php echo $Banner['slide_info'][$rand]['titlle']; ?></h1>
        <div class="subtitle" data-aos="fade-right" data-aos-delay="200"><?php echo $Banner['slide_info'][$rand]['subtitle']; ?></div>
        <?php if(array_key_exists('display_buttton',$Banner['slide_info'][$rand]) && $Banner['slide_info'][$rand]['display_buttton']==1){ ?>
            <a href="<?php echo $Banner['slide_info'][$rand]['button_link']; ?>" class="btn"  data-aos="fade-right" data-aos-delay="300"><?php echo $Banner['slide_info'][$rand]['button_label']; ?></a>
        <?php } ?>
    </div>
</div>



<div class="homepage-about" data-aos="fade-up">
    <div class="row content">
        <div class="col s12 l5 left">
            <div class="title-group text-left">
                <label data-aos="fade-right" data-aos-delay="100"><?php echo $Banner['about_label']; ?></label>
                <h2 data-aos="fade-right" data-aos-delay="200"><?php echo $Banner['about_title']; ?></h2>
            </div>
            <div class="subtitle text-gray"  data-aos="fade-right" data-aos-delay="300"><?php echo nl2br($Banner['about_description']); ?></div>
            <?php if(array_key_exists('display_button',$Banner) && $Banner['display_button']==1){ ?>
                <a href="<?php echo $Banner['about_button_link']['url']; ?>" class="btn waves-effect"  data-aos="fade-right" data-aos-delay="400"><?php echo $Banner['about_button_link']['title']; ?></a>
            <?php } ?>
        </div>
        <div class="col s12 m12 l7 right about-video hide-on-med-and-down">
            <a class="video" href="javascript:;"  data-aos="fade-up" data-aos-delay="500" style="background-image: url('<?php echo get_template_directory_uri().'/assets/images/qsp.jpg'; ?>')"></a>
            <?php /* ?>
            <?php for($i=1;$i<=4;$i++){ ?>
                <div class="image_<?php echo $i; ?>">
                    <div class="rellax" data-rellax-speed="<?php echo $i/2; ?>" style="background-image: url('<?php echo  $Banner['about_images_'.$i]['url']; ?>')"></div>
                </div>
            <?php } ?>
 <?php */ ?>
        </div>
    </div>
</div>


<div id="about-youtube-modal" class="modal"><div class="video-container"><?php echo wp_oembed_get($Banner['about_video_url']['url'], [ "width"=> "580"]); ?></div></div>




<div class="homepage-solutions parallax-window" data-parallax="scroll" data-image-src="<?php echo $Banner['solutions_background_image']; ?>">
    <div class="content">
        <div class="title-group text-center content-smaller">
            <label data-aos="fade-up" data-aos-delay="100"><?php echo $Banner['solutions_label']; ?></label>
            <h2 class="text-white"  data-aos="fade-up" data-aos-delay="200"><?php echo $Banner['solutions_title']; ?></h2>
        </div>
        <div class="subtitle text-white"  data-aos="fade-up" data-aos-delay="300"><?php echo $Banner['solutions_descriptionsolutions_description']; ?></div>

        <div class="cards">
        <?php $i=0; foreach ($Banner['solutions_highlighted'] AS $solution){ $i++; ?>
            <?php
            $solution_ID = extractQueruyValueFrmoURL("page_id",$solution);
            $PostInfo = get_post($solution_ID);
            $PostMeta = get_post_meta($solution_ID);
            ?>
            <a href="<?php echo $PostInfo->guid; ?>"  data-aos="fade-up" data-aos-delay="<?php echo 300+$i*50; ?>>">
                <div class="icon-main"><?php echo $PostMeta['icon'][0]; ?></div>
                <h2><?php echo $PostInfo->post_title; ?></h2>
                <div class="text text-gray"><?php echo $PostMeta['brief_intro'][0]; ?></div>
                <span class="material-icons arrow">arrow_forward</span>
            </a>

        <?php } ?>
        </div>

        <div class="line white opacity"></div>

        <div class="title-group text-left content-smaller left p20"  data-aos="fade-up" data-aos-delay="<?php echo 400+$i*50; ?>>">
            <h2 class="text-white"><?php echo $Banner['solutions_extravideo_title']; ?></h2>
        </div>


        <?php if(is_array($Banner['solutions_extra_solution_videos']) && array_key_exists("title",$Banner['solutions_extra_solution_videos']) && $Banner['solutions_extra_solution_videos']['title']!=''){ ?>
        <sep></sep>
        <div data-aos="fade-up" data-aos-delay="200">
        <a class="btn-watch text-white modal-trigger" href="#solutions-youtube-modal"><i class="fas fa-play"></i> <?php echo $Banner['solutions_extra_solution_videos']['title']; ?></a>
        </div>
        <?php }else{ ?>
        <sep></sep><sep></sep><?php } ?>
    </div>
</div>

<div id="solutions-youtube-modal" class="modal"><div class="video-container"><?php echo wp_oembed_get($Banner['solutions_extra_solution_videos']['url'], [ "width"=> "580"]); ?></div></div>





<div class="homepage-services" data-aos="fade-up" data-aos-delay="0">
    <div class="title-group text-center content-smaller">
        <label data-aos="fade-up" data-aos-delay="100">Services</label>
        <h2 class="" data-aos="fade-up" data-aos-delay="200">QSP Services</h2>
    </div>

    <div class="carousel-container">
        <div class="arrow-left" onclick="carouselPrev(this)"><i class="fas fa-chevron-left"></i></div>
        <div class="arrow-right" onclick="carouselNext(this)"><i class="fas fa-chevron-right"></i></div>
        <div class="carousel cards-carousel" style="min-height: 600px;" data-aos="fade-up" data-aos-delay="300">
            <?php foreach (get_pages([ "child_of" => 139, "parent" => 139, "order" => "menu_order" ]) AS $Service){ ?>
                <?php $PostMeta = get_post_meta($Service->ID); ?>
                <?php if($PostMeta['brief_intro'][0]!=''){ ?>
                    <a class="carousel-item" href="<?php echo $Service->guid; ?>">
                        <div class="image"><div style="background-image: url('<?php echo wp_get_attachment_image_src($PostMeta['main_image'][0])[0]; ?>');"></div></div>
                        <div class="inner">
                            <h2><?php echo $Service->post_title; ?></h2>
                            <div class="text text-gray"><?php echo $PostMeta['brief_intro'][0]; ?></div>
                        </div>
                        <div class="footer"><span>Learn More</span> <span class="material-icons arrow">arrow_forward</span></div>
                    </a>
                <?php } ?>
            <?php } ?>
            <?php foreach (get_pages([ "child_of" => 205, "parent" => 205, "order" => "menu_order" ]) AS $Service){ ?>
                <?php $PostMeta = get_post_meta($Service->ID); ?>
                <?php if($PostMeta['brief_intro'][0]!=''){ ?>
                    <a class="carousel-item" href="<?php echo $Service->guid; ?>">
                        <div class="image"><div style="background-image: url('<?php echo wp_get_attachment_image_src($PostMeta['main_image'][0])[0]; ?>');"></div></div>
                        <div class="inner">
                            <h2><?php echo $Service->post_title; ?></h2>
                            <div class="text text-gray"><?php echo $PostMeta['brief_intro'][0]; ?></div>
                        </div>
                        <div class="footer"><span>Learn More</span> <span class="material-icons arrow">arrow_forward</span></div>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>



<div class="homepage-partners">
    <div class="content">
        <div class="title-group text-center content-smaller">
            <label data-aos="fade-up" data-aos-delay="0">OUR PARTNERS</label>
            <h2 class=""data-aos="fade-up" data-aos-delay="100">Strategic & Long-term Partners</h2>
        </div>
        <div class="cards">
            <section class="text-center" data-aos="fade-up" data-aos-delay="200" style="width: calc(100% - 40px); background-color: transparent;">
                <h2  class="text-center">Strategic Partners</h2>
                <div class="text text-gray text-center">QSP has been working with se partners for many years, even decades:</div>
                <div class="logos">
                <?php foreach (get_posts([ "category_name" => "Strategic Partners"]) AS $partner){ $PostMeta = get_post_meta($partner->ID);  ?>
                    <div style="background-image: url('<?php echo wp_get_attachment_image_src($PostMeta['logo_gray'][0])[0]; ?>'); float:none; display: inline-block"></div>
                <?php } ?>
                <?php foreach (get_posts([ "category_name" => "Long-term Partners"]) AS $partner){ $PostMeta = get_post_meta($partner->ID);  ?>
                    <div style="background-image: url('<?php echo wp_get_attachment_image_src($PostMeta['logo_gray'][0])[0]; ?>'); float:none; display: inline-block"></div>
                <?php } ?>
                </div>
            </section>
            <?php /* ?>
            <section class="text-left" data-aos="fade-up" data-aos-delay="300"><h2  class="text-left">Long-term Partners</h2>
                <div class="text text-gray text-left">We are also dealer/distributor of the following suppliers:</div>
                <div class="logos">
                <?php foreach (get_posts([ "category_name" => "Long-term Partners"]) AS $partner){ $PostMeta = get_post_meta($partner->ID);  ?>
                    <div style="background-image: url('<?php echo wp_get_attachment_image_src($PostMeta['logo_gray'][0])[0]; ?>')"></div>
                <?php } ?>
                </div>
            </section>
 <?php */ ?>
        </div>
        <sep></sep><sep></sep>
        <a href="/?page_id=246" class="learn_more waves-effect " data-aos="fade-up" data-aos-delay="400"><span>Learn More</span> <span class="material-icons arrow">arrow_forward</span></a>
    </div>
</div>



<?php /*

<div class="homepage-testimonials parallax-window" data-parallax="scroll" data-image-src="<?php echo $Banner['testimonials_background_image']; ?>" >
    <div class="content">
        <div class="title-group text-center content-smaller">
            <label data-aos="fade-up" data-aos-delay="0"><?php echo $Banner['testimonials_label']; ?></label>
            <h2 class="text-white" data-aos="fade-up" data-aos-delay="100"><?php echo $Banner['testimonials_tittle']; ?></h2>
        </div>
        <div class="subtitle text-white" data-aos="fade-up" data-aos-delay="200"><?php echo $Banner['testimonials_description']; ?></div>
        <sep></sep><sep></sep><sep></sep><sep></sep>
        <div class="carousel cards-carousel-full dark" style="min-height: 300px;" data-aos="fade-up" data-aos-delay="300">
            <?php foreach (get_posts([ "category_name" => "Testimonial"]) AS $testimonial){ $PostMeta = get_post_meta($testimonial->ID);  ?>
                <section class="carousel-item" href="javascript:;">
                    <div class="image" style="background-image: url('<?php echo wp_get_attachment_image_src($PostMeta['image'][0])[0]; ?>')"></div>
                    <div class="right">
                        <h3><?php echo $PostMeta['full_name'][0]; ?></h3>
                        <div class=""><?php echo $PostMeta['position'][0]; ?></div>
                    </div>
                    <div class="text"><?php echo $PostMeta['testimonial'][0]; ?></div>
                </section>
            <?php } ?>
        </div>
    </div>
</div>


<?php */ ?>

<?php /*

<div class="homepage-articles" data-aos="fade-up" data-aos-delay="0">
    <div class="content">
        <div class="title-group text-center content-smaller">
            <label data-aos="fade-up" data-aos-delay="100">Blog</label>
            <h2 class="" data-aos="fade-up" data-aos-delay="200">Daily Articles</h2>
        </div>
        <div class="blog-posts">
            <?php $i=0; foreach (get_posts([ "category_name" => "blog-media", "numberposts" => 3]) AS $posts){ $PostMeta = get_post_meta($posts->ID); $i++; ?>
                <?php
                $PostMeta = get_post_meta($posts->ID);
                ?>
                <a href="<?php echo $PostInfo->guid; ?>" class="waves-effect" data-aos="fade-up" data-aos-delay="<?php echo 200+$i*100; ?>">
                    <div class="image" style="background-image: url('<?php echo wp_get_attachment_image_src($PostMeta['image'][0])[0]; ?>')"></div>
                    <div class="date"><span class="material-icons">event</span> <?php echo $posts->post_date; ?></div>
                    <h2><?php echo $posts->post_title; ?></h2>
                    <div class="text text-gray"><?php echo substr(strip_tags($posts->post_content), 0,150); ?> ...</div>
                    <sep></sep>
                    <div class="learn_more left "><span>Learn More</span> <span class="material-icons arrow">arrow_forward</span></div>
                </a>
            <?php } ?>
        </div>
    </div>
</div>

*/ ?>

<?php get_footer(); ?>
