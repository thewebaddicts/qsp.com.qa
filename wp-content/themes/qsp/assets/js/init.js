$(document).ready(function(){
    $('[data-youtube]').youtube_background();

    window.addEventListener('scroll', InitializeMenuScroll);
    function InitializeMenuScroll(){
        var scroll_start = 0;
        if (($(document).scrollTop() - 40) > 0){
            $('header.main_menu').addClass('compact');
        }else{
            $('header.main_menu').removeClass('compact');
        }
    }



    var rellax = new Rellax('.rellax', {
        speed: -2,
        center: false,
        wrapper: null,
        round: true,
        vertical: true,
        horizontal: false
    });


    $('.cards-carousel').carousel({
        dist: 0,
        indicators: true,
        padding:60,
        noWrap:true,
    });
    $('.cards-carousel-blue').carousel({
        dist: 0,
        indicators: true,
        padding:60,
        noWrap:true,
        // fullWidth: true
    });



    $('.cards-carousel-full').carousel({
        dist: 0,
        indicators: true,
        padding:60,
        noWrap:true,
        fullWidth: true
    });



    $('.accordion a').click(function(){
        $(this).parent().toggleClass('expanded');
    });

    $('.modal').modal({
        onCloseStart : function(elem){ $(elem).find('iframe').attr( 'src', function ( i, val ) { return val; }); }
    });
    AOS.init({ easing: 'ease-in-out-sine', duration: 1000, once : true });

    $('ul.mobile .menu-item-has-children a').click(function(){
        $(this).parent().find('ul.sub-menu').first().toggleClass('expanded');
    });
    $('.main_menu .toggle').click(function(){
        $(this).toggleClass('expanded');
        $('ul.mobile').toggleClass('expanded');
    })
});



function carouselPrev(elem){
    // $(elem).hide();
    var elems = $(elem).parents('.carousel-container').find('.carousel')
    var moveRight = M.Carousel.getInstance(elems);
    moveRight.prev(1);
}

function carouselNext(elem){
    // $(elem).hide();
    var elems = $(elem).parents('.carousel-container').find('.carousel')
    var moveRight = M.Carousel.getInstance(elems);
    moveRight.next(1);
}
