<?php

function qsp_theme_support(){
    //adds dynamic tags support
    add_theme_support('title-tag');
    add_theme_support('custom-logo');
}
add_action('after_setup_theme','qsp_theme_support');


function qsp_menu(){

    $locations = [
        'primary' => "Desktop Primary Menu",
        'footer' => "Desktop Footer Menu",
        'mobile' => "Mobile Menu"
    ];
    register_nav_menus($locations);
}

add_action('init','qsp_menu');

function qsp_register_styles(){

    wp_enqueue_style('qsp_font-awesome','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css', [], '5.15.1','all');
    wp_enqueue_style('qso_materializecss','https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css', [], '1.0','all');
    wp_enqueue_style('qsp_rellax',get_template_directory_uri().'/assets/js/rellax-master/css/main.css', [], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('qsp_main_style',get_template_directory_uri().'/style.css?v=1.2', ['qso_materializecss'], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('qsp_material','https://fonts.googleapis.com/icon?family=Material+Icons', [], wp_get_theme()->get('version'),'all');
    wp_enqueue_style('qsp_aos',get_template_directory_uri().'/assets/js/aos/aos.css', [], wp_get_theme()->get('version'),'all');
//    wp_enqueue_style('qsp_ytplayer_css',get_template_directory_uri().'/assets/js/ytplayer/css/jquery.mb.YTPlayer.min.css', [], '1.0','all');
}

add_action('wp_enqueue_scripts','qsp_register_styles');



function qsp_register_scripts(){
    wp_enqueue_script('qsp_jquery','https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.js', array(),'3.5.1',true);
    wp_enqueue_script('qsp_materializecss_js','https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js', array(),'1.0.0',true);
    wp_enqueue_script('qsp_ytplayer_js',get_template_directory_uri().'/assets/js/youtube-bg/jquery.youtube-background.js', array('qsp_jquery'),'1.0.0',true);
    wp_enqueue_script('qsp_rellax_js',get_template_directory_uri().'/assets/js/rellax-master/rellax.min.js', array(),'1.0.0',true);
    wp_enqueue_script('qsp_aos',get_template_directory_uri().'/assets/js/aos/aos.js',array(),'1.0.0',true);
    wp_enqueue_script('qsp_init',get_template_directory_uri().'/assets/js/init.js?v=1.0.3',array('qsp_ytplayer_js','qsp_materializecss_js'),'1.0.2',true);
    wp_enqueue_script('qsp_parallax',get_template_directory_uri().'/assets/js/parallax-js/parallax.min.js',array(),'1.0.0',true);
}

add_action('wp_enqueue_scripts','qsp_register_scripts');




function extractYoutubeID($URL){
    preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $URL, $matches);
    return $matches[0];

}


function extractQueruyValueFrmoURL($param, $URL){
    $parts = parse_url($URL);
    parse_str($parts['query'], $query);
    return $query[$param];
}



function show_page($id) {
    $post = get_post($id);
    $content = apply_filters('the_content', $post->post_content);
    echo $content;
}


