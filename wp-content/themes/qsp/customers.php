<?php /* Template Name: Template | Customers Page */ ?>
<?php get_header(); ?>
<?php $meta = get_post_meta(get_the_ID()); ?>



<?php //$Banner = get_fields('24'); ?>
<?php //$youtubeID = extractYoutubeID($Banner['video_url']); ?>

<div class="menu-spacer"></div>
<div class="content-banner parallax-window <?php if($meta['page_large_header'][0]!=1){ echo "small"; } ?> <?php if($meta['page_is_centered'][0]==1){ echo "text-center"; } ?>" data-parallax="scroll" data-image-src="<?php echo wp_get_attachment_image_src($meta['page_header_image'][0])[0]; ?>" data-aos="fade-in">
    <div class="content">
        <div class="banner-content">
            <h1 data-aos="fade-right" data-aos-delay="100"><?php echo $meta['page_header_title'][0]; ?></h1>
            <div  data-aos="fade-right" data-aos-delay="200" class="subtitle"><?php echo $meta['page_header_text'][0]; ?></div>
        </div>
    </div>
</div>


<div class="customers-list">
    <div class="content row">
        <?php for($i=0;$i<$meta['customers_list'][0];$i++){ ?>
            <div class="col s6 m4 l3"  data-aos="fade-up" data-aos-delay="<?php echo $i*100+200; ?>">
                <a class="logo" href="<?php echo $meta['customers_list_'.$i.'_website'][0]; ?>" target="_blank" title="<?php echo $meta['customers_list_'.$i.'_title'][0]; ?>" style="background-image: url('<?php echo wp_get_attachment_image_src($meta['customers_list_'.$i.'_logo'][0])[0]; ?>')"></a>
            </div>
        <?php } ?>
    </div>
</div>



<?php get_footer(); ?>


