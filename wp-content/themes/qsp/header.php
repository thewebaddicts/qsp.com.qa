<?php

require_once 'wp-gitlab-updater/theme-updater.php';

new Moenus\GitLabUpdater\ThemeUpdater( [
    'slug' => 'qsp-wordpress-theme',
    'access_token' => 'VsojUj7ksxLjLrn68Som',
    'gitlab_url' => 'gitlab.com:thewebaddicts/qsp-wordpress-theme.git',
    'repo' => '/thewebaddicts/qsp-wordpress-theme',
] );

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>
<body>
<?php $TopInfo = get_fields('24'); ?>
<header class="main_menu">
    <div class="top">
        <div class="content">
            <div class="left">
<!--                <a href="tel:--><?php //echo $TopInfo['telephone_number'] ?><!--" target="_blank"><b>T</b>--><?php //echo $TopInfo['telephone_number'] ?><!--</a>-->
<!--                <a href="mailto:--><?php //echo $TopInfo['email'] ?><!--" target="_blank"><b>E</b>--><?php //echo $TopInfo['email'] ?><!--</a>-->
            </div>
            <div class="right">
                <?php if(array_key_exists("facebook",$TopInfo) && array_key_exists("url",$TopInfo['facebook']) && $TopInfo['facebook']['url']!=''){ ?><a href="<?php echo $TopInfo['facebook']['url'] ?>" target="_blank"><?php echo htmlspecialchars_decode($TopInfo['facebook']['title']); ?></a><?php } ?>
                <?php if(array_key_exists("twitter",$TopInfo) && array_key_exists("url",$TopInfo['twitter']) && $TopInfo['twitter']!=''){ ?><a href="<?php echo $TopInfo['twitter']['url'] ?>" target="_blank"><?php echo htmlspecialchars_decode($TopInfo['twitter']['title']); ?></a><?php } ?>
                <?php if(array_key_exists("youtube",$TopInfo) && array_key_exists("url",$TopInfo['youtube']) && $TopInfo['youtube']!=''){ ?><a href="<?php echo $TopInfo['youtube']['url'] ?>" target="_blank"><?php echo htmlspecialchars_decode($TopInfo['youtube']['title']); ?></a><?php } ?>
                <?php if(array_key_exists("linkedin",$TopInfo) && array_key_exists("url",$TopInfo['linkedin']) && $TopInfo['linkedin']!=''){ ?><a href="<?php echo $TopInfo['linkedin']['url'] ?>" target="_blank"><?php echo htmlspecialchars_decode($TopInfo['linkedin']['title']); ?></a><?php } ?>
                <a href="https://www.midisgroup.com/" target="_blank" class="more_text"><?php if(array_key_exists("extra_text",$TopInfo) && $TopInfo['extra_text']!=''){  echo $TopInfo['extra_text']; } ?></a>
            </div>
        </div>
    </div>
    <nav>
        <div class="content">
            <a href="#" class="toggle"><i class="fas fa-bars"></i><i class="fas fa-times"></i></a>
        <?php if(function_exists('the_custom_logo')){ the_custom_logo(); } ?>
        <?php
        wp_nav_menu([
            "menu" => "primary",
            "theme_location" => "primary",
            "container" => "",
            "items_wrao" => "<ul></ul>",
        ]);




        wp_nav_menu([
            "menu" => "primary",
            "theme_location" => "primary",
            "container" => "",
            "items_wrao" => "<ul></ul>",
            "menu_class" => "mobile"
        ]);
        ?>
        </div>
    </nav>
</header>


<?php if(is_admin_bar_showing()){ ?>
<style>
    header.main_menu{ top:30px !important; }
</style>
<?php } ?>
